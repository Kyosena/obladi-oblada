from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView
from .forms import StatusForm
from .models import Statuses

# Create your views here.
def index(request):
    form = StatusForm()
    if request.method == 'POST':
        print('test')
        data = Statuses(status = request.POST['status'])
        data.save()
        return HttpResponseRedirect(reverse('confirm'))
    arguement = {
        'StatusList' : Statuses.objects.all(),
        'Statusform' : StatusForm
    }
    return render(request,"index.html",arguement)
def redirect(request):
    return HttpResponseRedirect('Story7/status/')
def confirm(request):
    arguement = {
        'StatusObject' : Statuses.objects.last()
    }
    return render(request,"confirm.html",arguement)
def delete(request):
    Statuses.objects.last().delete()
    return HttpResponseRedirect('../Story7/status/')
