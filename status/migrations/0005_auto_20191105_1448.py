# Generated by Django 2.2.5 on 2019-11-05 07:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('status', '0004_auto_20191105_1430'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statuses',
            name='status',
            field=models.CharField(default="I'm good", max_length=300),
        ),
    ]
